<div align="center"><img width="250" src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/17412335/Agimans-logo-simples.png"></div>

# Agimans - Agile Management Software
[Agimans](https://gitlab.com/projeto-de-desenvolvimento/agimans/api/-/wikis/home) é um sistema de gestão de projetos, que se baseia em metodologias ágeis, principalmente Scrum


## Instalação
### Instalação das Dependências
Recomendado utilizar **Node v12.17.0** para melhor reprodução do sistema

```bash
yarn
```


### Rodar o Projeto
Por padrão o projeto é reproduzido em <http://localhost:3000/>
#### Desenvolvimento
```bash
yarn start:dev
```

#### Gerar Build do Projeto
```bash
yarn build
```

#### Produção
```bash
yarn start
```


## Contato
- **E-Mail:** [brunoferrazsilveira@outlook.com.br](brunoferrazsilveira@outlook.com.br)
- **GitLab:** [brunofsilveira](https://gitlab.com/brunofsilveira)