import getConfig from 'next/config'

export const API_URL = getConfig().serverRuntimeConfig.API_URL || 'http://localhost:4000'
