import gql from 'graphql-tag'

export const CREATE_GROUP = gql`
  mutation createGroup($project: Boolean!, $name: String!, $slug: String!, $description: String!) {
    createOneGroup(input: { group: { project: $project, name: $name, slug: $slug, description: $description } }) {
      id
      name
      slug
      description
      project
    }
  }
`
