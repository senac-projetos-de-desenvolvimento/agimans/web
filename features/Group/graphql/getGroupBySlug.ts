import gql from 'graphql-tag'

export const GET_GROUP_BY_SLUG = gql`
  query getGroupBySlug($slug: String!) {
    groups(filter: { slug: { eq: $slug } }) {
      edges {
        node {
          id
          active
          name
          project
          slug
          description
          fatherGroup {
            id
            active
            name
            project
            slug
            description
          }
          childGroups {
            edges {
              node {
                id
                active
                name
                project
                slug
                description
              }
            }
          }
        }
      }
    }
  }
`
