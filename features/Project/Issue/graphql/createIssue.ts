import gql from 'graphql-tag'

export const CREATE_ISSUE = gql`
  mutation($title: String!, $description: String!, $estimate: Float, $spend: Float, $weight: Float) {
    createOneIssue(
      input: {
        issue: { title: $title, description: $description, estimate: $estimate, spend: $spend, weight: $weight }
      }
    ) {
      id
      active
      index
      title
      description
      estimate
      spend
      weight
    }
  }
`
