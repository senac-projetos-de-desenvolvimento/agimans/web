import gql from 'graphql-tag'

export const SET_SPRINT_LIST_AND_USER_TO_ISSUE = gql`
  mutation($issueId: ID!, $listId: ID!, $sprintId: ID!, $userId: ID!) {
    setListOnIssue(input: { id: $issueId, relationId: $listId }) {
      id
      active
      index
      title
      description
      estimate
      spend
      weight
    }
    setSprintOnIssue(input: { id: $issueId, relationId: $sprintId }) {
      id
      active
      index
      title
      description
      estimate
      spend
      weight
      sprint {
        lists(sorting: [{ field: index, direction: ASC }]) {
          edges {
            node {
              id
              active
              index
              name
              description
              issues(sorting: [{ field: index, direction: ASC }]) {
                edges {
                  node {
                    id
                    active
                    index
                    title
                    description
                    estimate
                    spend
                    weight
                  }
                }
              }
              status {
                id
                active
                color
                name
              }
            }
          }
        }
      }
    }
    addUsersToIssue(input: { id: $issueId, relationIds: [$userId] }) {
      id
      active
      index
      title
      description
      estimate
      spend
      weight
    }
  }
`
