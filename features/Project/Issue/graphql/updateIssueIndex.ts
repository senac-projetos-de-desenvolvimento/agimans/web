import gql from 'graphql-tag'

export const UPDATE_ISSUE_INDEX = gql`
  mutation($id: ID!, $index: Float!, $listId: ID!) {
    updateOneIssue(input: { id: $id, update: { index: $index } }) {
      id
      index
      title
      description
      estimate
      spend
      weight
    }
    setListOnIssue(input: { id: $id, relationId: $listId }) {
      id
      index
      list {
        id
        index
      }
    }
  }
`
