import gql from 'graphql-tag'

export const CREATE_LIST = gql`
  mutation($name: String!, $description: String!) {
    createOneList(input: { list: { name: $name, description: $description } }) {
      id
      active
      index
      name
    }
  }
`
