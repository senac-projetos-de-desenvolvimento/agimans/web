import gql from 'graphql-tag'

export const SET_STATUS_ON_LIST = gql`
  mutation($listId: ID!, $statusId: ID!) {
    setStatusOnList(input: { id: $listId, relationId: $statusId }) {
      id
      active
      index
      name
      description
      createdAt
      updatedAt
      deletedAt
    }
  }
`
