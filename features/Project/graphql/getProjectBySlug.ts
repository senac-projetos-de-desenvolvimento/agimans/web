import gql from 'graphql-tag'

export const GET_PROJECT_BY_SLUG = gql`
  query($slug: String!) {
    groups(filter: { project: { is: true }, slug: { eq: $slug } }) {
      edges {
        node {
          id
          active
          project
          name
          slug
          description
          sprints {
            edges {
              node {
                id
                active
                endDate
                startDate
                title
                description
                lists(sorting: [{ field: index, direction: ASC }]) {
                  edges {
                    node {
                      # index
                      id
                      active
                      name
                      description
                      issues(sorting: [{ field: index, direction: ASC }]) {
                        edges {
                          node {
                            # index
                            id
                            active
                            title
                            description
                            estimate
                            spend
                            weight
                          }
                        }
                      }
                      status {
                        id
                        active
                        color
                        name
                      }
                    }
                  }
                }
              }
            }
          }
          statuses {
            edges {
              node {
                id
                active
                color
                name
              }
            }
          }
          users {
            edges {
              node {
                active
                email
                name
              }
            }
          }
        }
      }
    }
  }
`
