import React from 'react'
import Head from 'next/head'

import { ApolloProvider } from '@apollo/react-hooks'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { NextPageContext, NextPage } from 'next'
import { ThemeProvider } from 'styled-components'

import GlobalStyle from '../shared/components/common/GlobalStyle'
import Signin from './signin'

import { AgimansCookie, getAgimansCookie } from '../shared/utils/cookies'
import { setTheme } from '../shared/common/theme'
import { ThemeType } from '../shared/common/theme/types'
import { withApollo } from '../apollo/client'

interface Props {
  agimansCookie: AgimansCookie
  agimansCookieExists: boolean
  theme: ThemeType
  Component?: any
  pageProps?: any
  apollo?: any
}

const App: NextPage<Props> = ({ agimansCookie, agimansCookieExists, theme, Component, pageProps, apollo }: Props) => {
  return (
    <>
      <ApolloProvider client={apollo}>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <DndProvider backend={HTML5Backend}>
            <Head>
              <title>Agimans</title>
              <meta name="viewport" content="initial-scale=1.0, width=device-width" />
              <link rel="icon" type="image/png" sizes="32x32" href="/assets/icons/logo-agimans.svg"></link>
              <link rel="icon" type="image/png" sizes="16x16" href="/assets/icons/logo-agimans.svg"></link>
              <link
                href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;700&display=swap"
                rel="stylesheet"
              />
            </Head>
            {!agimansCookieExists && <Signin />}
            {agimansCookieExists && <Component {...pageProps} agimansCookie={agimansCookie} />}
          </DndProvider>
        </ThemeProvider>
      </ApolloProvider>
    </>
  )
}

App.getInitialProps = async (ctx: NextPageContext): Promise<Props> => {
  const [agimansCookie, agimansCookieExists] = getAgimansCookie(ctx)
  const theme = setTheme()

  return { agimansCookie, agimansCookieExists, theme }
}

export default withApollo(App)
