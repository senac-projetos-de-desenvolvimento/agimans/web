import React, { useEffect, useState } from 'react'

import { useMutation, useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/router'

import CardList from '../../../shared/components/Card/CardList'
import Form, { buttonTypes, formHTMLElement } from '../../../shared/components/Form'
import MainAgimans from '../../../shared/components/Main/MainAgimans'
import SubHeader from '../../../shared/components/SubHeader/SubHeader'
import SubHeaderLink from '../../../shared/components/SubHeader/SubHeaderLink'

import { AgimansCookie } from '../../../shared/utils/cookies'
import { cardTypes } from '../../../shared/components/Card/Card'
import { Container } from '../../../shared/components/common/styled'
import { ToastError, ToastSuccess } from '../../../shared/utils/Toast'
import { withApollo } from '../../../apollo/client'

import { ADD_CHILD_GROUPS_TO_GROUP } from '../../../features/Group/graphql/addChildGroupsToGroup'
import { ADD_GROUPS_TO_USER } from '../../../features/User/graphql/addGroupsToUser'
import { CREATE_GROUP } from '../../../features/Group/graphql/createGroup'
import { GET_GROUP_BY_SLUG } from '../../../features/Group/graphql/getGroupBySlug'
import { GET_USER_BY_EMAIL } from '../../../features/User/graphql/getUserByEmail'
import { SET_FATHER_GROUP_ON_GROUP } from '../../../features/Group/graphql/setFatherGroupOnGroup'

interface Props {
  agimansCookie?: AgimansCookie
}

function SlugGroup({ agimansCookie }: Props) {
  const [groups, setGroups] = useState([])

  const router = useRouter()
  const routerQuery = router.query

  const [addChildGroupsToGroupMutation] = useMutation(ADD_CHILD_GROUPS_TO_GROUP)
  const [addGroupsToUserMutation] = useMutation(ADD_GROUPS_TO_USER)
  const [createGroupMutation] = useMutation(CREATE_GROUP)
  const [setFatherGroupOnGroupMutation] = useMutation(SET_FATHER_GROUP_ON_GROUP)

  const getUserByEmail = useQuery(GET_USER_BY_EMAIL, {
    variables: { email: agimansCookie.userCookie.email },
  })

  const getGroupBySlug = useQuery(GET_GROUP_BY_SLUG, {
    variables: { slug: routerQuery.slugGroup },
  })

  useEffect(() => {
    if (!getGroupBySlug.loading) {
      setGroups(getGroupBySlug.data.groups.edges[0].node.childGroups.edges)
    }
  }, [getGroupBySlug.loading])

  async function handleSubmit(event: any) {
    event.preventDefault()

    const nameElement = String(event.currentTarget.elements.name.value)
    const descriptionElement = String(event.currentTarget.elements.description.value)
    const projectElement = Boolean(event.currentTarget.elements.project.checked)
    const slugElement = String(event.currentTarget.elements.slug.value)

    try {
      const createGroup = await createGroupMutation({
        variables: {
          project: projectElement,
          name: nameElement,
          slug: slugElement,
          description: descriptionElement,
        },
      })

      await addGroupsToUserMutation({
        variables: {
          userId: getUserByEmail.data.users.edges[0].node.id,
          groupId: [createGroup.data.createOneGroup.id],
        },
      })

      const addChildGroupsToGroup = await addChildGroupsToGroupMutation({
        variables: {
          groupId: getGroupBySlug.data.groups.edges[0].node.id,
          childId: [createGroup.data.createOneGroup.id],
        },
      })

      await setFatherGroupOnGroupMutation({
        variables: {
          groupId: createGroup.data.createOneGroup.id,
          fatherId: getGroupBySlug.data.groups.edges[0].node.id,
        },
      })

      setGroups(addChildGroupsToGroup.data.addChildGroupsToGroup.childGroups.edges)
      ToastSuccess(
        `${projectElement ? 'Projeto' : 'Grupo'} ${createGroup.data.createOneGroup.name} criado com sucesso!`,
      )

      formHTMLElement.reset()
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <MainAgimans showHeader>
      <SubHeader>
        <SubHeaderLink path="/" name="Meus Grupos e Projetos" />
      </SubHeader>
      <Container>
        <CardList className="grid-cards" cards={groups} type={cardTypes.groupOrProject} />
        <Form
          handleSubmit={handleSubmit}
          title="Criar Grupo"
          inputs={[
            { id: 'name', name: 'name', type: 'name', autoComplete: 'name', required: true, placeholder: 'Nome...' },
            {
              id: 'description',
              name: 'description',
              type: 'text',
              autoComplete: 'description',
              required: true,
              placeholder: 'Descrição...',
            },
            { id: 'slug', name: 'slug', type: 'text', autoComplete: 'slug', required: true, placeholder: 'URL...' },
            { id: 'project', name: 'project', type: 'checkbox', placeholder: 'É um Projeto?' },
          ]}
          buttons={[
            { name: 'Criar Grupo ou Projeto', type: buttonTypes.submit },
            { name: 'Concelar', type: buttonTypes.reset },
          ]}
        />
      </Container>
    </MainAgimans>
  )
}

export default withApollo(SlugGroup)
