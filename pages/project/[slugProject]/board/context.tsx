import { createContext } from 'react'

export default createContext({
  moveToCard: Function,
  moveToList: Function,
  lists: Array,
})
