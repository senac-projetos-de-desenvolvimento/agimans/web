import React from 'react'
import ReactMarkdown from 'react-markdown'

import { useMutation } from '@apollo/react-hooks'

import { buttonTypes } from '../../../../../shared/components/Form'
import { FormButton, FormContainer } from '../../../../../shared/components/Form/styles'
import { IssueContent, MarkdownContent } from '../../../../../shared/components/Card/styles'
import { IssueDetailsInterface } from '../index'
import { ToastError, ToastSuccess } from '../../../../../shared/utils/Toast'

import { ADD_USER_TO_ISSUE } from '../../../../../features/Project/Issue/graphql/addUserToIssue'

interface Props {
  issueDetails: IssueDetailsInterface
  showModalIssueToUpdate: any
  setShowIssueModal: any
  userId: number
}

export default function FormIssueDetails({ issueDetails, showModalIssueToUpdate, setShowIssueModal, userId }: Props) {
  const [addUsersToIssueMutation] = useMutation(ADD_USER_TO_ISSUE)

  console.log('issueDetails', issueDetails)

  async function addUsersToIssue(issueId: number, userId: number) {
    try {
      const addUsersToIssue = await addUsersToIssueMutation({
        variables: {
          issueId,
          userId,
        },
      })

      ToastSuccess(`História ${addUsersToIssue.data.addUsersToIssue.title} vinculada ao teu usuário com sucesso!`)
      setShowIssueModal(false)
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <FormContainer>
      <IssueContent>
        {issueDetails?.title && <h2>{issueDetails.title}</h2>}
        <MarkdownContent>
          <ReactMarkdown source={issueDetails.description} />
        </MarkdownContent>
        <FormButton
          className="full-width"
          type={buttonTypes.button}
          onClick={() => addUsersToIssue(issueDetails.id, userId)}
        >
          Vincular
        </FormButton>
        <FormButton
          className="modal-issue-button"
          type={buttonTypes.button}
          onClick={() => showModalIssueToUpdate(issueDetails)}
        >
          Editar Issue
        </FormButton>
        <FormButton className="modal-issue-button" type={buttonTypes.button} onClick={() => setShowIssueModal(false)}>
          Sair
        </FormButton>
      </IssueContent>
    </FormContainer>
  )
}
