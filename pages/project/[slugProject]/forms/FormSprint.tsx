import React from 'react'

import { useMutation } from '@apollo/react-hooks'

import Form, { buttonTypes } from '../../../../shared/components/Form'

import { ToastError, ToastSuccess } from '../../../../shared/utils/Toast'

import { ADD_SPRINT_TO_PROJECT } from '../../../../features/Project/graphql/addSprintToProject'
import { CREATE_SPRINT } from '../../../../features/Project/Sprint/graphql/createSprint'

interface Props {
  project: any
  setSprints: any
  setShowSprintForm: any
}

export default function FormSprint({ project, setSprints, setShowSprintForm }: Props) {
  const [addSprintToProjectMutation] = useMutation(ADD_SPRINT_TO_PROJECT)
  const [createSprintMutation] = useMutation(CREATE_SPRINT)

  async function handleSubmitSprint(event: any) {
    event.preventDefault()

    const endDateElement = String(event.currentTarget.elements.endDate.value).toLocaleString()
    const startDateElement = String(event.currentTarget.elements.startDate.value).toLocaleString()
    const titleElement = String(event.currentTarget.elements.title.value)
    const descriptionElement = String(event.currentTarget.elements.description.value)

    try {
      const createSprint = await createSprintMutation({
        variables: {
          endDate: endDateElement,
          startDate: startDateElement,
          title: titleElement,
          description: descriptionElement,
        },
      })

      const addSprintToProject = await addSprintToProjectMutation({
        variables: {
          projectId: project.id,
          sprintId: [createSprint.data.createOneSprint.id],
        },
      })

      setSprints(addSprintToProject.data.addSprintsToGroup.sprints.edges)

      ToastSuccess(`Sprint ${createSprint.data.createOneSprint.title} criado com sucesso!`)
      setShowSprintForm(false)
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <Form
      handleSubmit={handleSubmitSprint}
      title="Criar Sprint"
      inputs={[
        {
          id: 'title',
          name: 'title',
          type: 'text',
          autoComplete: 'title',
          required: true,
          placeholder: 'Título...',
        },
        {
          id: 'description',
          name: 'description',
          type: 'text',
          autoComplete: 'description',
          required: true,
          placeholder: 'Descrição...',
        },
        {
          id: 'startDate',
          name: 'startDate',
          type: 'datetime-local',
          autoComplete: 'startDate',
          required: true,
          label: 'Início',
        },
        {
          id: 'endDate',
          name: 'endDate',
          type: 'datetime-local',
          autoComplete: 'endDate',
          required: true,
          label: 'Final',
        },
      ]}
      buttons={[
        { type: buttonTypes.submit, name: 'Criar Sprint' },
        { type: buttonTypes.reset, onClick: () => setShowSprintForm(false), name: 'Cancelar' },
      ]}
    />
  )
}
