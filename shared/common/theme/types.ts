export type FontType = {
  primary: string
  light: string
  lightLess: string
  medium: string
  darkLess: string
  dark: string
}

export type ThemeType = {
  primary: string
  primary75: string
  primary50: string
  secondary: string
  tertiary: string
  light: string
  dark: string
  dark10: string
  mainBackground: string
  componentBackground: string
  inputBackground: string
  shadow: string
  gradient: {
    primaryVertical: string
    primaryHorizontal: string
    primaryForm: string
    secondary: string
    grayScale: string
    darkScale: string
  }
  default: string
  font: FontType
}

export interface ThemeInterface {
  light: ThemeType
  dark: ThemeType
}
