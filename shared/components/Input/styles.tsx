import styled from 'styled-components'

interface ContainerProps {
  haveBackground?: boolean
}

interface InputProps {
  haveBackground?: boolean
}

export const StyledInputContainer = styled.div<ContainerProps>`
  margin: 10px;
  border-radius: 5px;

  div {
    padding: 5px 10px;
    width: 360px;
    border-radius: 5px 5px 0 0;
    z-index: 5;

    &.date-time {
      background-color: ${({ theme }) => theme.inputBackground};
    }

    p {
      color: ${({ theme }) => theme.font.medium};
      border-bottom: 1px solid ${({ theme }) => theme.dark};
      font-size: 13.5px;
      width: 100%;
      height: 100%;
      margin-bottom: -9px;
      cursor: default;
    }
  }

  div + input {
    border-radius: 0 0 5px 5px;
  }

  textarea {
    background-color: ${({ theme }) => theme.inputBackground};
    min-width: 360px;
    max-width: 360px;
    min-height: 150px;
    border-radius: 5px;
    border: none;
    padding: 5px 10px;
    overflow-y: hidden;
  }
`

export const StyledInput = styled.input<InputProps>`
  background-color: ${({ theme }) => theme.inputBackground};
  width: 360px;
  height: 40px;
  border-radius: 5px;
  border: none;
  padding: 5px 10px;

  &::placeholder {
    color: ${({ theme }) => theme.font.medium};
  }

  &::-webkit-datetime-edit {
    color: ${({ theme }) => theme.font.medium};
    text-transform: uppercase;
    font-size: 13px;
  }

  &::-webkit-datetime-edit-fields-wrapper {
  }

  &::-webkit-datetime-edit-text {
    padding: 0 0.3em;
  }

  &::-webkit-datetime-edit-month-field {
  }

  &::-webkit-datetime-edit-day-field {
  }

  &::-webkit-datetime-edit-year-field {
  }

  &::-webkit-inner-spin-button {
    display: none;
  }

  &::-webkit-calendar-picker-indicator {
    display: none;
    color: ${({ theme }) => theme.font.medium};

    &:hover {
      color: ${({ theme }) => theme.font.darkLess};
      background: none;
    }
  }
`
