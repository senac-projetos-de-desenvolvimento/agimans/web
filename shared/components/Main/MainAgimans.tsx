import React, { ReactNode } from 'react'
import styled from 'styled-components'

import Header from '../Header/Header'
import Sidebar from '../Sidebar/Sidebar'

const MainContainer = styled.main`
  display: grid;
  grid-template-areas:
    'sidebar header'
    'sidebar content';
  width: 100vw;
  height: 100vh;

  &.show-header {
    grid-template-columns: 50px 1fr;
    grid-template-rows: 50px 1fr;
  }

  &.not-show-header {
    grid-template-columns: 0 1fr;
    grid-template-rows: 0 1fr;
  }
`

const Article = styled.article`
  grid-area: content;
  position: relative;
`

interface Props {
  children: ReactNode
  sidebarChildren?: ReactNode
  showHeader?: boolean
}

const MainAgimans = (props: Props) => {
  return (
    <>
      {props.showHeader && (
        <MainContainer className="show-header">
          <Sidebar>{props.sidebarChildren}</Sidebar>
          <Header />
          <Article>{props.children}</Article>
        </MainContainer>
      )}
      {!props.showHeader && (
        <MainContainer className="not-show-header">
          <Article>{props.children}</Article>
        </MainContainer>
      )}
    </>
  )
}

export default MainAgimans
