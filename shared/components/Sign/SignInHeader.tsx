import React from 'react'
import styled from 'styled-components'

const SignInContainer = styled.div`
  grid-area: 'header';
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.font.light};
`

const Logo = styled.img`
  width: 100px;
  height: 100px;
`

const H1 = styled.h1`
  font-size: 35px;
  font-weight: 300;
  margin: 10px 10px 5px 10px;
`

const H2 = styled.h2`
  font-size: 25px;
  font-weight: 400;
  margin: 5px 10px 10px 10px;
`

export default function SignInHeader() {
  return (
    <SignInContainer>
      <Logo src="/assets/icons/logo-agimans.svg" alt="Logo Agimans" />
      <H1>Agimans</H1>
      <H2>Agile Management Software</H2>
    </SignInContainer>
  )
}
