import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'

const SubHeaderLinkContainer = styled.a`
  font-size: 15px;
  text-decoration: none;
  color: ${({ theme }) => theme.font.light};
`

interface Props {
  path: string
  name: string
  as?: string
}

const SubHeaderLink = (props: Props) => {
  return (
    <Link href={props.path} as={props.as}>
      <SubHeaderLinkContainer>{props.name}</SubHeaderLinkContainer>
    </Link>
  )
}

export default SubHeaderLink
