import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'

const IconContainer = styled.div`
  grid-area: header;
  width: 40px;

  a {
    text-decoration: none;
    color: ${({ theme }) => theme.font.light};
  }

  p {
    text-align: center;
    font-size: 8px;
    font-weight: bold;
  }
`

interface Props {
  imgPath: string
  text: string
  as?: string
  path: string
}

function Icon(props: Props) {
  function redirect() {
    return
  }

  return (
    <IconContainer>
      <Link href={props.path} as={props.as}>
        <a onClick={() => redirect()}>
          <img src={props.imgPath} alt="" />
          <p>{props.text}</p>
        </a>
      </Link>
    </IconContainer>
  )
}

export default Icon
