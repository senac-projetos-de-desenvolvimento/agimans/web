import styled from 'styled-components'

export const SignInBackground = styled.div`
  height: 100vh;
  width: 100vw;
  background-image: url('/assets/imgs/sign-in-background.png');
  background-repeat: no-repeat;
  background-size: cover;
  display: grid;
  grid-template-areas: 'left right';
  grid-template-rows: 1fr;
  grid-template-columns: 1fr 1fr;
`

export const Left = styled.div`
  padding: 58px;
  grid-area: 'left';
`

export const Right = styled.div`
  padding: 50px;
  grid-area: 'right';
  display: grid;
  grid-template-areas:
    'header'
    'form'
    'footer';
  grid-template-rows: min-content 1fr min-content;
  grid-template-columns: 1fr;
  background: ${({ theme }) => theme.gradient.darkScale};
  background-blend-mode: multiply;
`

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const Button = styled.button`
  background-color: ${({ theme }) => theme.secondary};
  color: ${({ theme }) => theme.font.light};
  width: 167px;
  height: 40px;
  margin: 10px;
  border: none;
  border-radius: 5px;
  font-size: 15px;
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.gradient.secondary};
    background-blend-mode: multiply;
  }
`

export const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`

export const BoardContainer = styled.div`
  position: absolute;
  display: flex;
  height: calc(100% - 120px);
  width: 100%;
`

export const FiltersContainer = styled.div`
  width: calc(100% - 20px);
  height: 50px;
  padding: 10px;
  margin: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  .styled-select-container {
    height: 45px;
    width: 160px;
    background-color: ${({ theme }) => theme.primary};
    border-radius: 10px;
    padding: 5px;
  }

  .button-container {
    display: inline-flex;

    > div {
      margin-left: 20px;
    }
  }

  .add-button-container {
    width: 45px;
    height: 45px;
    background-color: ${({ theme }) => theme.primary};
    border-radius: 10px;
    padding: 5px;
    display: flex;
    justify-content: center;
    align-items: center;

    &.label {
      width: auto;
    }
  }
`

export const StyledSelect = styled.select`
  font-size: 18px;
  font-weight: 400;
  line-height: 35px;
  width: 150px;
  padding: 0 10px;
  background-color: ${({ theme }) => theme.componentBackground};
  border-radius: 10px;
  border: 0;
  outline: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  color: ${({ theme }) => theme.primary};
  transition: all 0.4s ease-in-out;

  &::after {
    width: 5px;
    height: 5px;
    background-color: yellow;
  }

  &:hover {
    cursor: pointer;
  }

  option {
    border: none;
    border-bottom: 1px solid ${({ theme }) => theme.default};
    padding: 10px;
    -webkit-appearance: none;
    -moz-appearance: none;
  }
`

export const CardContainer = styled.div<{ color: string }>`
  background-color: ${({ theme }) => theme.componentBackground};
  border-top: 5px solid ${({ theme, color }) => (color ? color : theme.default)};
  border-left: 5px solid ${({ theme, color }) => (color ? color : theme.default)};
  border-radius: 10px;
  min-width: 250px;
  max-width: 450px;
  height: min-content;
  max-height: 300px;
  margin: 10px 10px 20px 10px;
  padding: 10px;
  box-shadow: 0 1px 4px 0 ${({ theme }) => theme.shadow};

  p {
    color: ${({ theme, color }) => (color ? color : theme.default)};
  }

  &.group,
  &.issue,
  &.list {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    flex-direction: column;

    a {
      font-size: 18px;
      text-decoration: none;
      color: ${({ theme, color }) => (color ? color : theme.default)};
      font-weight: 400;
      cursor: pointer;
    }
  }

  &.group,
  &.issue {
    height: 100px;

    p {
      font-size: 18px;
      color: ${({ theme }) => theme.font.darkLess};
    }
  }

  &.issue,
  &.list {
    width: 350px;
  }

  &.list {
    font-size: 22px;
    color: ${({ theme }) => theme.default};
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
  }

  &.group {
    width: 400px;
  }

  &.issue {
    cursor: grab;

    a {
      color: ${({ theme }) => theme.font.darkLess};
      font-weight: bold;
    }
  }

  &.sprint {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    flex-direction: column;

    .status {
    }

    .dates {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-top: 10px;
      width: 100%;
    }

    p {
      font-size: 16px;
      color: ${({ theme }) => theme.font.darkLess};

      span {
        font-weight: bold;
      }
    }
  }
`

export const CardStatus = styled.div<{ color: string }>`
  background-color: ${(props) => props.color};
  color: ${({ theme }) => theme.font.light};
  width: 100px;
  height: 25px;
  line-height: 25px;
  border-radius: 12.5px;
  margin: 10px;
  font-size: 12px;
  padding: 0 5px;

  p {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    text-align: center;
  }
`

export const ListButton = styled.a<{ color }>`
  width: 25px;
  height: 25px;
  border: none;
  background-color: transparent;
  cursor: pointer;
  color: ${({ theme, color }) => (color ? color : theme.default)};
`

export const AddButton = styled.button`
  border: none;
  border-radius: 10px;
  background-color: ${({ theme }) => theme.componentBackground};
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  &.label {
    p {
      color: ${({ theme }) => theme.primary};
      font-weight: bold;
      padding: 5px;
    }

    img {
      margin-right: 5px;
    }
  }
`
