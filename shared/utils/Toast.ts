import Swal from 'sweetalert2'

import { getErrorMessage } from './form'
import { GraphQLError } from 'graphql'

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  },
})

export const ToastError = (error: GraphQLError): void => {
  Toast.fire({
    icon: 'error',
    title: String(getErrorMessage(error)).replace('GraphQL error: ', ''),
  })
}

export const ToastSuccess = (message: string): void => {
  Toast.fire({
    icon: 'success',
    title: message,
  })
}

export const ToastWarning = (message: string): void => {
  Toast.fire({
    icon: 'warning',
    title: message,
  })
}

export const ToastInfo = (message: string): void => {
  Toast.fire({
    icon: 'info',
    title: message,
  })
}

export const ToastQuestion = (message: string): void => {
  Toast.fire({
    icon: 'question',
    title: message,
  })
}
